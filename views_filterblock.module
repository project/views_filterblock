<?php

/** @file
 * A simple module that uses javascript to reposition the views placeholder
 * in a block
 */ 

include_once(drupal_get_path('module', 'views') .'/views_cache.inc');

/**
 * Implementation of hook_menu()
 */
function views_filterblock_menu($may_cache) {
  $items['admin/settings/views_filterblock'] = array(
    'title' => t('Views Filter Block'),
    'description' => t('Move the views filter form to a block.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => 'views_filterblock_settings',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM, // optional
  );
  return $items;
}

function views_filterblock_settings() {
  $form['views_filterblock_num'] = array(
    '#type' => 'select',
    '#title' => t('Number of Blocks'),
    '#options' => drupal_map_assoc(range(1, 10)),
    '#default_value' => variable_get('views_filterblock_num', 1),
    '#description' => t('Select the number of blocks'),
  );
  return system_settings_form($form);
}

/**
 * Implementation of hook_block().
 */
function views_filterblock_block($op = 'list', $delta = 0, $edit = array() ) {
  if ($op == 'list') {
    for ($i = 0; $i < variable_get('views_filterblock_num', 1); $i ++) {
      $blocks[$i]['info'] = t('Views Filter Block.!i', array('!i' => $i));
    }
    return $blocks;
  }
  elseif ($op == 'configure') {
    $options[''] = '-- Select One --';
    $result = db_query('SELECT * FROM {view_view} WHERE page=1');
    while ($view = db_fetch_object($result)) {
      $options[$view->name] = $view->description ? $view->description : $view->name;
      $used[$view->name] = true;
    }
    $default_views = _views_get_default_views();
    foreach ($default_views as $name => $view) {
      if ($view->page && !$used[$view->name]) {
        $options[$view->name] = $view->description;
      }
    }
    asort($options);

    $var = 'views_filterblock_view';
    $form[$var] = array(
      '#type' => 'select',
      '#title' => t('View'),
      '#options' => $options,
      '#default_value' => variable_get("$var.$delta", ''),
      '#description' => t('Select a View whose filter you want to display in the block'),
    );
    return $form;
  }
  elseif ($op == 'save') {
    $var = 'views_filterblock_view';
    variable_set("$var.$delta", $edit[$var]);
  } 
  elseif ($op == 'view') {
    $block['content'] = _views_filterblock_block($delta);
    return $block;
  }
}

function _views_filterblock_block($delta) {
  // TODO: this needs to be rewritten for Drupal 5.0
  if ($name = variable_get("views_filterblock_view.$delta", '')) {
    if ($view = views_get_view($name)) {
      return drupal_get_form('views_filterblock', $view);
    }
  }
}

function views_filterblock($view) {
  $form = views_filters($view);
  $form['#action'] = url($view->url);
  $form['#views_filterblock'] = true;
  return $form;
}

function views_filterblock_theme() {
  return array(
    'views_filterblock' => array(
      'arguments' => array('form'),
    ),
    'views_filterblock_output' => array(
      'arguments' => array('form'),
    ),
  );
}

function theme_views_filterblock($form) {
  $view = $form['view']['#value'];

  // make the 'q' come first
  $output = drupal_render($form['q']);

  foreach ($view->exposed_filter as $count => $filter) {
    $newform["fieldset$count"] = array(
      '#type' => 'fieldset',
      '#title' => $filter['label'],
      '#collapsible' => true,
      '#weight' => $count - 1000, // we'll never have this many filters
    );
    $newform["fieldset$count"]['#collapsed'] = TRUE;
  }

  foreach ($form as $field => $value) {
    if (preg_match('/(op|filter)([0-9]+)/', $field, $match)) {
      $curcount = $match[2];
      $newform["fieldset$curcount"][$field] = $value;

      if (!isset($newform["fieldset$curcount"]['#weight'])) {
        $newform["fieldset$curcount"]['#weight'] = $value['#weight'];
      }
    }
    else {
      if ($field == 'submit' || drupal_substr($field, 0, 1) == '#') {
        unset($curcount);
      }
      if (isset($curcount)) {
        $newform["fieldset$curcount"][$field] = $value;
      }
      else {
        $newform[$field] = $value;
      }
    }
  }

  foreach ($view->exposed_filter as $count => $filter) {
    if ($filter['is_default']) {
      $newform["fieldset$count"]['#collapsed'] = FALSE;
      $open = TRUE;
    }
    else {
      $value = $newform["fieldset$count"]["filter$count"]['#default_value'];
      if (isset($value)) {
        if (is_array($value)) {
          foreach ($value as $key => $item) {
            if ($item != '') {
              $newform["fieldset$count"]['#collapsed'] = FALSE;
              $open = TRUE;
            }
          }
        }
        elseif ($value != '') {
          $newform["fieldset$count"]['#collapsed'] = FALSE;
          $open = TRUE;
        }
      }
    }
  }
  if (!$open) {
    $newform["fieldset0"]['#collapsed'] = FALSE;
  }

  return theme('views_filterblock_output', $newform);
}

function theme_views_filterblock_output($form) {
  return drupal_render($form);
}

/**
 * Implementation of hook_form_alter().
 */
function views_filterblock_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'views_filters') {
    /**
     * Hide the form displayed as part of the view, when the
     * filterform is displayed in a block
     */
    $name = $form['view']['#value']->name;
    if (!$form['#views_filterblock']) {
      foreach (array('content', 'left', 'right', 'header', 'footer') as $region) {
        foreach (block_list($region) as $block) {
          if ($block->module == 'views_filterblock' && variable_get("views_filterblock_view.$block->delta", '') == $name) {
            $form['#type'] = 'hidden';
            return;
          }
        }
      }
    }
  }
}
